#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>

using namespace cv;
using namespace std;

/// Global Variables
Mat src; Mat hsv; Mat hue;
int bins = 20;
Mat src_gray;
int thresh = 100;
int max_thresh = 255;
RNG rng(12345);
double largest_area =0;
double largest_contour_index = 1000000;
Ptr<BackgroundSubtractor> pMOG2; //MOG2 Background subtractor

/// Function Headers
void Hist_and_Backproj(int, void* );
void DetectConvexHull(Mat src );

/** @function main */
int main( int argc, char** argv )
{
    vector<String> filenames;
    String folder = "/home/rickeshtn/Hackathon/HandDetect/Pics/";
    cv::Mat img;
    Mat threshold_output;
    cv::VideoCapture cap("/home/rickeshtn/Hackathon/HandDetect/Test.mp4");
    std::cerr << cap.isOpened() << std::endl;
    cv::glob(folder, filenames); // new function that does the job ;-)

        Mat3b frame;
        while(cap.read(frame))
        {
            cv::Mat src = frame;
            resize(src, img, Size(), 0.7, 0.7, INTER_CUBIC); // upscale 2x
            cv::imshow("Src", img);
            /* THRESHOLD ON HSV*/
            cvtColor(frame, frame, CV_BGR2HSV);
            GaussianBlur(frame, frame, Size(7,7), 1, 1);
            //medianBlur(frame, frame, 15);
            for(int r=0; r<frame.rows; ++r){
                for(int c=0; c<frame.cols; ++c)
                    // 0<H<0.25  -   0.15<S<0.9    -    0.2<V<0.95
                    if( (frame(r,c)[0]>5) && (frame(r,c)[0] < 17) && (frame(r,c)[1]>38) && (frame(r,c)[1]<250) && (frame(r,c)[2]>51) && (frame(r,c)[2]<242) ); // do nothing
                    else for(int i=0; i<3; ++i)	frame(r,c)[i] = 0;
            }

            /* BGR CONVERSION AND THRESHOLD */
            Mat1b frame_gray;
            cvtColor(frame, frame, CV_HSV2BGR);
            cvtColor(frame, frame_gray, CV_BGR2GRAY);
            threshold(frame_gray, frame_gray, 60, 255, CV_THRESH_BINARY);
            morphologyEx(frame_gray, frame_gray, CV_MOP_ERODE, Mat1b(3,3,1), Point(-1, -1), 3);
            morphologyEx(frame_gray, frame_gray, CV_MOP_OPEN, Mat1b(7,7,1), Point(-1, -1), 1);
            morphologyEx(frame_gray, frame_gray, CV_MOP_CLOSE, Mat1b(9,9,1), Point(-1, -1), 1);

            medianBlur(frame_gray, frame_gray, 15);
            imshow("Threshold", frame_gray);

            cvtColor(frame, frame, CV_BGR2HSV);
            resize(frame, frame, Size(), 0.5, 0.5);
            imshow("Video",frame);

            DetectConvexHull(frame);
            waitKey(5);
        }

  waitKey(0);
  return 0;
}


/**
 * @function Hist_and_Backproj
 * @brief Callback to Trackbar
 */

void DetectConvexHull(Mat src )
 {
   cout<<"1 "<<endl;
   Mat src_copy = src.clone();
   cvtColor(src,src_gray,CV_BGR2GRAY);
   Mat threshold_output;
   vector<vector<Point> > contours;
   vector<Vec4i> hierarchy;

   /// Detect edges using Threshold
   threshold( src_gray, threshold_output, thresh, 255, THRESH_BINARY );

   /// Find contours
   findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    cout<<"2 "<<endl;
   /// Find the convex hull object for each contour
   vector<vector<Point> >hull( contours.size() );
   for( int i = 0; i < contours.size(); i++ )
      {
       double Area = contourArea(contours[i],false);
       if(Area>1000)
            convexHull( Mat(contours[i]), hull[i], false );
      }

   /// Draw contours + hull results
   Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
   for( int i = 0; i< contours.size(); i++ )
      {
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        //drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
        drawContours( drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
      }

   /// Show in a window
   namedWindow( "Hull demo", CV_WINDOW_AUTOSIZE );
   imshow( "Hull demo", drawing );
 }

void Hist_and_Backproj(int, void* )
{
  MatND hist;
  Mat threshold_output;
  int histSize = MAX( bins, 2 );
  float hue_range[] = { 0, 180 };
  const float* ranges = { hue_range };

  /// Get the Histogram and normalize it
  calcHist( &hue, 1, 0, Mat(), hist, 1, &histSize, &ranges, true, false );
  normalize( hist, hist, 0, 255, NORM_MINMAX, -1, Mat() );

  /// Get Backprojection
  MatND backproj;
  calcBackProject( &hue, 1, 0, hist, backproj, &ranges, 1, true );

  /// Draw the backprojProj
  string winName1 = "back";
  namedWindow(winName1,CV_WINDOW_NORMAL);
  imshow( "BackProj", backproj );



//  vector<vector<Point> > contours;
//  vector<Vec4i> hierarchy;


//  /// Detect edges using Threshold
//  threshold( backproj, threshold_output, thresh, 255, THRESH_BINARY );

//  //Dilate image
//  dilate(threshold_output,threshold_output,7);
//  /// Find contours
//  findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

//  /// Find the convex hull object for each contour
//  vector<vector<Point> >hull( contours.size() );
//  for( int i = 0; i < contours.size(); i++ )
//  {
//      double area = contourArea(contours[i],false);
//      if(area>100)
//      {
//          convexHull( Mat(contours[i]), hull[i], false );
//      }
//  }


//  /// Draw contours + hull results
//  Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
//  for( int i = 0; i< contours.size(); i++ )
//  {
//      Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
//      //drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
//      drawContours( drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
//  }

//  namedWindow( "Hull demo", CV_WINDOW_AUTOSIZE );
//  imshow( "Hull demo", drawing );

  /// Draw the histogram
  int w = 400; int h = 400;
  int bin_w = cvRound( (double) w / histSize );
  Mat histImg = Mat::zeros( w, h, CV_8UC3 );

  for( int i = 0; i < bins; i ++ )
     { rectangle( histImg, Point( i*bin_w, h ), Point( (i+1)*bin_w, h - cvRound( hist.at<float>(i)*h/255.0 ) ), Scalar( 0, 0, 255 ), -1 ); }
  string winName = "Histogram";
  namedWindow(winName,CV_WINDOW_OPENGL);
  resizeWindow(winName, 500,500);
  imshow( winName, histImg );
}
