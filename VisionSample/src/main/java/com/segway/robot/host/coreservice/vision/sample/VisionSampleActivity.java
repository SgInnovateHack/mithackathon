package com.segway.robot.host.coreservice.vision.sample;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.Message;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.speech.tts.Voice;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;
//Vision part
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.segway.robot.algo.dts.DTSPerson;
import com.segway.robot.sdk.base.bind.ServiceBinder;
import com.segway.robot.sdk.vision.DTS;
import com.segway.robot.sdk.vision.Vision;
import com.segway.robot.sdk.vision.frame.Frame;
import com.segway.robot.sdk.vision.stream.StreamInfo;
import com.segway.robot.sdk.vision.stream.StreamType;

import com.segway.robot.sdk.locomotion.sbv.AngularVelocity;
import com.segway.robot.sdk.locomotion.sbv.Base;
import com.segway.robot.sdk.locomotion.sbv.LinearVelocity;

//Speech part
import com.segway.robot.sdk.voice.Languages;
import com.segway.robot.sdk.voice.Recognizer;
import com.segway.robot.sdk.voice.Speaker;
import com.segway.robot.sdk.voice.VoiceException;
import com.segway.robot.sdk.voice.audiodata.RawDataListener;
import com.segway.robot.sdk.voice.grammar.GrammarConstraint;
import com.segway.robot.sdk.voice.grammar.Slot;
import com.segway.robot.sdk.voice.recognition.RecognitionListener;
import com.segway.robot.sdk.voice.recognition.RecognitionResult;
import com.segway.robot.sdk.voice.recognition.WakeupListener;
import com.segway.robot.sdk.voice.recognition.WakeupResult;
import com.segway.robot.sdk.voice.tts.TtsListener;


import android.app.ActionBar;
import android.app.Activity;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.segway.robot.sdk.base.bind.ServiceBinder;
import com.segway.robot.sdk.voice.Languages;
import com.segway.robot.sdk.voice.Recognizer;
import com.segway.robot.sdk.voice.Speaker;
import com.segway.robot.sdk.voice.VoiceException;
import com.segway.robot.sdk.voice.audiodata.RawDataListener;
import com.segway.robot.sdk.voice.grammar.GrammarConstraint;
import com.segway.robot.sdk.voice.grammar.Slot;
import com.segway.robot.sdk.voice.recognition.RecognitionListener;
import com.segway.robot.sdk.voice.recognition.RecognitionResult;
import com.segway.robot.sdk.voice.recognition.WakeupListener;
import com.segway.robot.sdk.voice.recognition.WakeupResult;
import com.segway.robot.sdk.voice.tts.TtsListener;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


/**
 * The Sample Activity demonstrate the main function of Segway Robot VisionService.
 */


public class VisionSampleActivity extends Activity implements CompoundButton.OnCheckedChangeListener {

    private boolean mBind;
    private Vision mVision;

    private Switch mBindSwitch;
    private Switch mPreviewSwitch;
    private Switch mTransferSwitch;

    private SurfaceView mColorSurfaceView;
    private SurfaceView mDepthSurfaceView;

    private ImageView mColorImageView;
    private ImageView mDepthImageView;

    private Bitmap mColorBitmap;
    private Bitmap mHBitmap;
    private Bitmap mSBitmap;
    private Bitmap mVBitmap;
    private Bitmap mDepthBitmap;

    private static final String TAG = "Activity";
    //Listening part
    private static final int APPEND = 0x000f;
    private static final int CLEAR = 0x00f0;
    private ServiceBinder.BindStateListener mRecognitionBindStateListener;
    private ServiceBinder.BindStateListener mSpeakerBindStateListener;
    private boolean isBeamForming = false;
    private boolean bindSpeakerService;
    private boolean bindRecognitionService;
    private int mSpeakerLanguage;
    private int mRecognitionLanguage;
    private Button mBindButton;
    private Button mUnbindButton;
    private Button mSpeakButton;
    private Button mStopSpeakButton;
    private Button mStartRecognitionButton;
    private Button mStopRecognitionButton;
    private Button mBeamFormListenButton;
    private Button mStopBeamFormListenButton;
    private Switch mEnableBeamFormSwitch;
    private TextView mStatusTextView;
    private Recognizer mRecognizer;
    private Speaker mSpeaker;
    private WakeupListener mWakeupListener;
    private RecognitionListener mRecognitionListener;
    private RawDataListener mRawDataListener;
    private TtsListener mTtsListener;
    private GrammarConstraint mTwoSlotGrammar;
    private GrammarConstraint mThreeSlotGrammar;

    private VoiceHandler mHandler = new VoiceHandler(this);
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public static class VoiceHandler extends Handler {
        private final WeakReference<Activity> mActivity;

        private VoiceHandler(Activity instance) {
            mActivity = new WeakReference<>(instance);
        }

        @Override
        public void handleMessage(Message msg) {
            Activity mainActivity = mActivity.get();
            if (mainActivity != null) {
                super.handleMessage(msg);
                switch (msg.what) {
                }
            }
        }
    }

    private void addEnglishGrammar() throws VoiceException, RemoteException {
        String grammarJson = "{\n" +
                "         \"name\": \"play_media\",\n" +
                "         \"slotList\": [\n" +
                "             {\n" +
                "                 \"name\": \"play_cmd\",\n" +
                "                 \"isOptional\": false,\n" +
                "                 \"word\": [\n" +
                "                     \"play\",\n" +
                "                     \"close\",\n" +
                "                     \"pause\"\n" +
                "                 ]\n" +
                "             },\n" +
                "             {\n" +
                "                 \"name\": \"media\",\n" +
                "                 \"isOptional\": false,\n" +
                "                 \"word\": [\n" +
                "                     \"the music\",\n" +
                "                     \"the video\"\n" +
                "                 ]\n" +
                "             }\n" +
                "         ]\n" +
                "     }";
        mTwoSlotGrammar = mRecognizer.createGrammarConstraint(grammarJson);
        mRecognizer.addGrammarConstraint(mTwoSlotGrammar);
        mRecognizer.addGrammarConstraint(mThreeSlotGrammar);

    }

    private void addChineseGrammar() throws VoiceException, RemoteException {
        Slot play = new Slot("play", false, Arrays.asList("播放", "打开", "关闭", "暂停"));
        Slot media = new Slot("media", false, Arrays.asList("音乐", "视频", "电影"));
        List<Slot> slotList = new LinkedList<>();
        slotList.add(play);
        slotList.add(media);
        mTwoSlotGrammar = new GrammarConstraint("play_media", slotList);
        mRecognizer.addGrammarConstraint(mTwoSlotGrammar);
        mRecognizer.addGrammarConstraint(mThreeSlotGrammar);
    }

    private void initListeners() {

        mRecognitionBindStateListener = new ServiceBinder.BindStateListener() {
            @Override
            public void onBind() {
                try {
                    //get recognition language when service bind.
                    mRecognitionLanguage = mRecognizer.getLanguage();

                    switch (mRecognitionLanguage) {
                        case Languages.EN_US:
                            addEnglishGrammar();
                            break;
                        case Languages.ZH_CN:
                            addChineseGrammar();
                            break;
                    }
                } catch (VoiceException | RemoteException e) {

                }
                bindRecognitionService = true;
                if (bindSpeakerService) {
                    //both speaker service and recognition service bind, enable function buttons.

                    mUnbindButton.setEnabled(true);
                }
            }

            @Override
            public void onUnbind(String s) {
                //speaker service or recognition service unbind, disable function buttons.
            }
        };

        mSpeakerBindStateListener = new ServiceBinder.BindStateListener() {
            @Override
            public void onBind() {
                try {
                    //get speaker service language.
                    mSpeakerLanguage = mSpeaker.getLanguage();
                } catch (VoiceException e) {

                }
                bindSpeakerService = true;
                if (bindRecognitionService) {
                    //both speaker service and recognition service bind, enable function buttons.

                }
            }

            @Override
            public void onUnbind(String s) {

            }
        };

        mWakeupListener = new WakeupListener() {
            @Override
            public void onStandby() {

            }

            @Override
            public void onWakeupResult(WakeupResult wakeupResult) {
                //show the wakeup result and wakeup angle.

            }

            @Override
            public void onWakeupError(String s) {
                //show the wakeup error reason.

            }
        };

        mRecognitionListener = new RecognitionListener() {
            @Override
            public void onRecognitionStart() {

            }

            @Override
            public boolean onRecognitionResult(RecognitionResult recognitionResult) {
                //show the recognition result and recognition result confidence.

                String result = recognitionResult.getRecognitionResult();

                if (result.contains("hello") || result.contains("hi")) {
                    try {
                        mRecognizer.removeGrammarConstraint(mThreeSlotGrammar);
                    } catch (VoiceException e) {

                    }
                    //true means continuing to recognition, false means wakeup.
                    return true;
                }
                return false;
            }

            @Override
            public boolean onRecognitionError(String s) {
                //show the recognition error reason.


                return false; //to wakeup
            }
        };

        mRawDataListener = new RawDataListener() {
            @Override
            public void onRawData(byte[] bytes, int i) {

            }
        };

        mTtsListener = new TtsListener() {
            @Override
            public void onSpeechStarted(String s) {
                //s is speech content, callback this method when speech is starting.

            }

            @Override
            public void onSpeechFinished(String s) {
                //s is speech content, callback this method when speech is finish.
            }

            @Override
            public void onSpeechError(String s, String s1) {
                //s is speech content, callback this method when speech occurs error.

            }
        };

    }


    ServiceBinder.BindStateListener mBindStateListener = new ServiceBinder.BindStateListener() {
        @Override
        public void onBind() {
            mBind = true;
        }

        @Override
        public void onUnbind(String reason) {
            mBind = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vision_sample);
        //Listener is On
        initListeners();//Init the listener

        // init content view
        mBindSwitch = (Switch) findViewById(R.id.bind);
        mPreviewSwitch = (Switch) findViewById(R.id.preview);
        mTransferSwitch = (Switch) findViewById(R.id.transfer);

        mBindSwitch.setOnCheckedChangeListener(this);
        mPreviewSwitch.setOnCheckedChangeListener(this);
        mTransferSwitch.setOnCheckedChangeListener(this);

        mColorSurfaceView = (SurfaceView) findViewById(R.id.colorSurface);
        mDepthSurfaceView = (SurfaceView) findViewById(R.id.depthSurface);

        mColorImageView = (ImageView) findViewById(R.id.colorImage);
        mDepthImageView = (ImageView) findViewById(R.id.depthImage);

        // get Vision SDK instance
        mVision = Vision.getInstance();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mBindSwitch.setChecked(false);
        mPreviewSwitch.setChecked(false);
        mTransferSwitch.setChecked(false);
    }

    /**
     * Start preview color and depth image
     */
    private synchronized void startPreview() {
        // 1. Get activated stream info from Vision Service.
        //    Streams are pre-config.
        StreamInfo[] infos = mVision.getActivatedStreamInfo();
        for (StreamInfo info : infos) {
            // Adjust image ratio for display
            float ratio = (float) info.getWidth() / info.getHeight();
            ViewGroup.LayoutParams layout;
            switch (info.getStreamType()) {
                case StreamType.COLOR:
                    // Adjust color surface view
                    mColorSurfaceView.getHolder().setFixedSize(info.getWidth(), info.getHeight());
                    layout = mColorSurfaceView.getLayoutParams();
                    layout.width = (int) (mColorSurfaceView.getHeight() * ratio);
                    mColorSurfaceView.setLayoutParams(layout);

                    // preview color stream
                    mVision.startPreview(StreamType.COLOR, mColorSurfaceView.getHolder().getSurface());
                    break;
                case StreamType.DEPTH:
                    // Adjust depth surface view
                    mDepthSurfaceView.getHolder().setFixedSize(info.getWidth(), info.getHeight());
                    layout = mDepthSurfaceView.getLayoutParams();
                    layout.width = (int) (mDepthSurfaceView.getHeight() * ratio);
                    mDepthSurfaceView.setLayoutParams(layout);

                    // preview depth stream
                    mVision.startPreview(StreamType.DEPTH, mDepthSurfaceView.getHolder().getSurface());
                    break;
            }
        }
    }

    /**
     * Stop preview
     */
    private synchronized void stopPreview() {
        StreamInfo[] infos = mVision.getActivatedStreamInfo();
        for (StreamInfo info : infos) {
            switch (info.getStreamType()) {
                case StreamType.COLOR:
                    // Stop color preview
                    mVision.stopPreview(StreamType.COLOR);
                    break;
                case StreamType.DEPTH:
                    // Stop depth preview
                    mVision.stopPreview(StreamType.DEPTH);
                    break;
            }
        }
    }

    /**
     * FrameListener instance for get raw image data form vision service
     */
    Vision.FrameListener mFrameListener;

    {
        mFrameListener = new Vision.FrameListener() {
            @Override
            public void onNewFrame(int streamType, Frame frame) {
                Runnable runnable = null;

                switch (streamType) {
                    case StreamType.COLOR:
                        // draw color image to bitmap and display
                        mColorBitmap.copyPixelsFromBuffer(frame.getByteBuffer());
                        runnable = new Runnable() {
                            @Override
                            public void run() {
                                //mColorImageView.setImageBitmap(mColorBitmap);
                                DTS dtrackS = mVision.getDTS();

// set video source
                                dtrackS.setVideoSource(DTS.VideoSource.CAMERA);

                                dtrackS.start();

// detect person in 3 seconds
                                DTSPerson[] persons = dtrackS.detectPersons(3 * 1000 * 1000);
                                if (persons.length > 0) if (persons[0].getDistance() < 10) {
                                    Log.d(TAG, "Detected one person");
                                    ///perfrom skin detection on the person

                                    int picw = mColorBitmap.getWidth();
                                    int pich = mColorBitmap.getHeight();
                                    int[] pix = new int[picw * pich];
                                    int w = mColorBitmap.getWidth();
                                    int h = mColorBitmap.getHeight();
                                    int[] mapSrcColor = new int[w * h];
                                    int[] mapDestColor = new int[w * h];
                                    float[] pixelHSV = new float[3];

                                    mHBitmap.setWidth(picw);
                                    mHBitmap.setHeight(pich);

                                    mSBitmap.setWidth(picw);
                                    mSBitmap.setHeight(pich);

                                    mVBitmap.setWidth(picw);
                                    mVBitmap.setHeight(pich);

                                    mColorBitmap.getPixels(mapSrcColor, 0, w, 0, 0, w, h);

                                    int index = 0;
                                    for (int y = 0; y < h; ++y) {
                                        for (int x = 0; x < w; ++x) {

                                            //Convert from Color to HSV
                                            Color.colorToHSV(mapSrcColor[index], pixelHSV);
                                            if((pixelHSV[0] >0 && pixelHSV[0] <20)
                                                    && (pixelHSV[1] >10 && pixelHSV[1] <150)
                                                    && (pixelHSV[2] >60 && pixelHSV[2] <255))
                                            {
                                                Log.d(TAG,"YES I SEE SKIN");
                                                
                                            }
                                            else
                                            {
                                                Log.d(TAG,"NO it is not skin");
                                            }

                                            mHBitmap.setPixel(x, y, (int)pixelHSV[0]);
                                            mSBitmap.setPixel(x, y, (int)pixelHSV[1]);
                                            mVBitmap.setPixel(x, y, (int)pixelHSV[2]);

                                            index++;
                                        }
                                    }
                                    //Start detecting for skin from the HSV images

                                    //one person is nearby.
                                    //Iterate on this if the person is tracked person is going for the tablet or not
                                } else {
                                    Log.d(TAG, "Go near the person detected");
                                    //Go near the person detected
                                    //Invoke Locomotion




                                }
                                else {
                                    //Move around looking for any person
                                    //Listen for voice
                                    Log.d(TAG, "Look for anyone in room");

                                }
                            }
                        };
                        break;
                    case StreamType.DEPTH:
                        // draw depth image to bitmap and display
                        mDepthBitmap.copyPixelsFromBuffer(frame.getByteBuffer());
                        runnable = new Runnable() {
                            @Override
                            public void run() {
                                mDepthImageView.setImageBitmap(mDepthBitmap);
                            }
                        };
                        break;
                }

                if (runnable != null) {
                    runOnUiThread(runnable);
                }
            }
        };
    }


    /**
     * Start transfer raw image data form VisionService to giving FrameListener
     */
    private synchronized void startImageTransfer() {
        StreamInfo[] infos = mVision.getActivatedStreamInfo();
        for (StreamInfo info : infos) {
            switch (info.getStreamType()) {
                case StreamType.COLOR:
                    mColorBitmap = Bitmap.createBitmap(info.getWidth(), info.getHeight(), Bitmap.Config.ARGB_8888);
                    mVision.startListenFrame(StreamType.COLOR, mFrameListener);
                    break;
                case StreamType.DEPTH:
                    mDepthBitmap = Bitmap.createBitmap(info.getWidth(), info.getHeight(), Bitmap.Config.RGB_565);
                    mVision.startListenFrame(StreamType.DEPTH, mFrameListener);
                    break;
            }
        }

    }

    /**
     * Stop transfer raw image data
     */
    private synchronized void stopImageTransfer() {
        mVision.stopListenFrame(StreamType.COLOR);
        mVision.stopListenFrame(StreamType.DEPTH);
    }

    /**
     * Buttons
     *
     * @param buttonView
     * @param isChecked
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.bind:
                if (isChecked) {
                    if (!mVision.bindService(this, mBindStateListener)) {
                        mBindSwitch.setChecked(false);
                        Toast.makeText(this, "Bind service failed", Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(this, "Bind service success", Toast.LENGTH_SHORT).show();
                } else {
                    mPreviewSwitch.setChecked(false);
                    mTransferSwitch.setChecked(false);
                    mVision.unbindService();
                    mBind = false;
                    Toast.makeText(this, "Unbind service", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.preview:
                if (isChecked) {
                    if (!mBind) {
                        mPreviewSwitch.setChecked(false);
                        Toast.makeText(this, "Need to bind service first", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    startPreview();
                } else {
                    if (mBind) {
                        stopPreview();
                    }
                }
                break;
            case R.id.transfer:
                if (isChecked) {
                    if (!mBind) {
                        mTransferSwitch.setChecked(false);
                        Toast.makeText(this, "Need to bind service first", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    startImageTransfer();
                } else {
                    if (mBind) {
                        stopImageTransfer();
                    }
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        if (mBind) {
            mVision.unbindService();
            StreamInfo[] infos = mVision.getActivatedStreamInfo();
            for (StreamInfo info : infos) {
                switch (info.getStreamType()) {
                    case StreamType.COLOR:
                        mVision.stopListenFrame(StreamType.COLOR);
                        break;
                    case StreamType.DEPTH:
                        mVision.stopListenFrame(StreamType.DEPTH);
                        break;
                }
            }
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("VisionSample Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }
}